﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    public Enemy EnemyLight;
    public Enemy EnemyHeavy;
    public bool canMove = false;
   


	// Use this for initialization
	void Start () {

        EnemyLight = new Enemy("Light", 1.0f, "Cubo");
        EnemyHeavy = new Enemy("Heavy", 2.0f, "Cubo");
         
		
	}

	// Update is called once per frame
	void Update () {

      

        if (canMove == true)
        {
            GetComponent<MoveEnemyto>().moveTo();
        }

        Morir();
	}

	public void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player")
		{
            //EnemyHeavy.vida--;
            //EnemyLight.vida--;
            canMove = true;
            GetComponent<MoveEnemyto>().targeto = other.gameObject.transform;

		}
	}
	public void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag == "Player")
		{
           
			//GameObject.Find("Player").GetComponent<Player>().isinvul = true;
			//GetComponent<EnemyActions>().Atacar();
		}
	}

	public void Morir(){
		if (this.gameObject.tag == "Light" && EnemyLight.vida <= 0) {
            GetComponent<EnemyBehaviour>().DestroyEnemy();
            if (EnemyLight.DropChance ()) {
                GetComponent<RandomVis>().Potioninsta();
            }
		}
		else if (this.gameObject.tag == "Heavy" && EnemyHeavy.vida <= 0) {
            GetComponent<EnemyBehaviour>().DestroyEnemy();
			if (EnemyHeavy.DropChance ()) {
                GetComponent<RandomVis>().Potioninsta();
            }
		}
	}
}
