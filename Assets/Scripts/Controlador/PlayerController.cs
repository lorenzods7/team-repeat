﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour 
{


	public Player jugador;
	public Potion potion;
	public PlayeMove playerMove;
	public StaminBar stamina;


	public void   Start()
	{
		jugador = new Player (6, "Capsule"); 
		potion = new Potion ();
	}



	public void Update()
	{

		if (Input.GetKeyDown (KeyCode.S)) 
		{  
			jugador.Atacar (1);
			jugador.HacerDmg (1);
			stamina.ReduceStamina (1);

			Debug.Log ("Atacando con espada");
		}

		if (Input.GetKeyDown (KeyCode.A)) 
		{ 
			jugador.Atacar (2);
			jugador.HacerDmg (2);
			stamina.ReduceStamina (2);
			Debug.Log ("Atacando con Hacha");

		}
		// 
		if (Input.GetKey (KeyCode.UpArrow))
		{
			playerMove.Mover(new Vector3 (0,0,1),transform);
			Debug.Log ("avanzando hacia adelante");
		}

		if (Input.GetKey (KeyCode.DownArrow)) 
		{
			playerMove.Mover(new Vector3 (0,0,-1),transform);
			Debug.Log ("avanzando hacia abajo");
		}

		if (Input.GetKey (KeyCode.LeftArrow)) 
		{
			playerMove.Mover(new Vector3 (-1,0,0),transform);
			Debug.Log ("avanzando hacia la izq");
		}

		if (Input.GetKey (KeyCode.RightArrow)) 
		{
			playerMove.Mover(new Vector3 (1,0,0),transform);
			Debug.Log ("avanzando hacia la derecha");
		}

	}


	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.CompareTag("potion"))
		{
			potion.ResVida (jugador.vida);
		}
	}


	public void muerte()
	{

		if (jugador.vida <= 0 ) 
		{
			Destroy (gameObject);

		}

	}




}
