﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arma : Item {

	public int dmg;

	public Arma(){
	}

	public Arma(int dmg, string name){
		this.dmg = dmg;
		this.name = name;
	}

}
