﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Humano {

	public string nombre;
	public string shape;
	public float vida;
	public int staminacount;
	Arma arma;

	public virtual int Atacar(int drain){
		staminacount = staminacount - drain;
		return staminacount;
	}

	public float HacerDmg(float dmg){
		vida = vida - dmg;
		return vida;
	}
		
}
