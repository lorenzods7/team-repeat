﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Humano {

	public GameObject target = null;
	public float speed;
	private int _health;
	private int _damage;
	public int drop;

	public int health
	{
		get {
			return _health;
		}
		set {
			_health = value;
			if (_health < 0)
			{
				_health = 0;
			}
		}
	}
	public int damage
	{
		get { return _damage; }
	}

	public Enemy(int _health, int _dmg)
	{
		health = _health;
		_damage = _dmg;
	}

	public void TakeDamage(int _dmg)
	{
		health -= _dmg;
	}


	public bool DropChance(){
		drop = Random.Range (0, 100);
		return drop <= 20;
	}

	void ReachPlayer(){
	}

	enum Enemigos {Ligero, Pesado};


	public Enemy(string nombre, float vida, string shape){
		this.nombre = nombre;
		this.vida = vida;
		this.shape = "";
	}


	public override int Atacar (int drain) // 
	{
		return base.Atacar (drain);

//	public override void Atacar ()
	//{
		
	//	base.Atacar ();


		//vista.attac ();

	//}
		


	/*void OnTriggerEnter (Collider other){
		if (other == "player")
			target = "player";
	} 

	void OnTriggerExit (Collider other){
		if (other == "player")
			target = null;
	} */

	//void OnTriggerEnter (Collider other){
	//	if (other == player)
	//		target = player;
	//} 

	//void OnTriggerExit (Collider other){
	//	if (other == player)
	//		target = null;
	//} 

	}
}
