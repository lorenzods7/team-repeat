﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayeMove : MonoBehaviour {

	public float speed = 6.0F;
	
	public void Mover (Vector3 movimiento, Transform actor)
	{
		actor.forward = movimiento;
		actor.position += movimiento  * speed*Time.deltaTime;
		
	}
}
