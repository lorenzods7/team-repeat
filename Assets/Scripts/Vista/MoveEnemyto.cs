﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveEnemyto : MonoBehaviour
{

   public float speed;
   public Transform targeto;

    public void moveTo()
       
    {
        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, targeto.position, step);
        transform.LookAt(targeto);
        

    } 
}