﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour{


    public GameObject Resume;

    public void pauseGame()
    {
        Time.timeScale = 0;
        Resume.SetActive(true);
       
    }
    public void Resumebut()
    {
        Time.timeScale = 1;
        Resume.SetActive(false);
        
    }
    public void MainMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
