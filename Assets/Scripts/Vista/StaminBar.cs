﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaminBar : MonoBehaviour
{

    public List<Image> cuitos = new List<Image>(); // Crea una lista para la barra de vida

    public Color azul = Color.blue;
    public Color amarillo = Color.yellow;
	public Humano humanoInUse;

    // Use this for initialization
    void Start()
    {

        for (int i = 0; i < cuitos.Count; i++)
        {
            cuitos[i].color = azul;

        }

        humanoInUse.staminacount = cuitos.Count;
    }
// Update is called once per frame
void Update()
{

	if (Input.GetKeyDown(KeyCode.KeypadPlus)) // prueba de barra
		AddStamina();


	if (Input.GetKeyDown(KeyCode.KeypadMinus)) // prueba de barra
		ReduceStamina(1);


}

public void AddStamina()
{
	humanoInUse.staminacount++;

	if (humanoInUse.staminacount > cuitos.Count)
		humanoInUse.staminacount = cuitos.Count;

	cuitos[humanoInUse.staminacount - 1].color = azul;
}

public void ReduceStamina(int value)
{
		for (int i = 0; i < value; i++) {
			humanoInUse.staminacount--;
			if (humanoInUse.staminacount < 0)
				humanoInUse.staminacount = 0;
			cuitos[humanoInUse.staminacount].color = amarillo;
		}	
			
	
}

}