﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public Canvas MainCanvas;
    public Canvas CreditsCanvas;
    public Canvas ExitCanvas;

    void Awake()
    {

        CreditsCanvas.enabled = false;
        ExitCanvas.enabled = false;
    }
    public void CreditsOn ()
    {
        MainCanvas.enabled = false;
        CreditsCanvas.enabled = true;
        ExitCanvas.enabled = false;
    }

    public void ReturnOn()
    {
        MainCanvas.enabled = true;
        CreditsCanvas.enabled = false;
        ExitCanvas.enabled = false;
    }

    public void LoadGame()
    {
        SceneManager.LoadScene(1);
        
    }
    public void Exit()
    {
        MainCanvas.enabled = false;
        CreditsCanvas.enabled = false;
        ExitCanvas.enabled = true;

    }
    public void ExitGame()

    {
        Application.Quit();
    }
}