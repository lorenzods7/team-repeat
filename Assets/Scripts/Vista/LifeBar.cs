﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeBar : MonoBehaviour {

    public List<Image> cuitos = new List<Image>(); // Crea una lista para la barra de vida

    public Color rojo = Color.red;
    public Color gris = Color.grey;
    public int lifecount;

	// Use this for initialization
	void Start () {
		
        for (int i = 0; i< cuitos.Count; i++)
        {
            cuitos[i].color = rojo;

        }

        lifecount = cuitos.Count;
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.KeypadPlus)) // prueba de barra
            AddLife();


        if (Input.GetKeyDown(KeyCode.KeypadMinus)) // prueba de barra
            ReduceLife();


    }

    public void AddLife()
    {
        lifecount++;

        if (lifecount > cuitos.Count)
            lifecount = cuitos.Count;

        cuitos[lifecount -1].color = rojo;
    }

    public void ReduceLife()
    {
        lifecount--;
        if (lifecount < 0)
            lifecount = 0;
        cuitos[lifecount].color = gris;
    }

}
