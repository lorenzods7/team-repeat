﻿using UnityEngine;
using System.Collections;

public class EnemyBehaviour : MonoBehaviour {

    public Enemy enemy = new Enemy(1, 1);

    public void ReceiveDamage(int _dmg)
    {
		enemy.vida -= _dmg;
        CheckIfDead();
    }

    void CheckIfDead()
    {
		if(enemy.vida <= 0)
        {
            DestroyEnemy();
        }
    }
  
    public void DestroyEnemy()
    {
        Destroy(gameObject);
    }
}
