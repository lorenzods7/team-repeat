﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmaVista : MonoBehaviour {

	Arma arma;
	public void OnTriggerEnter(Collider other)
    {
        Enemy enemy = other.GetComponent<Enemy>();
        if (enemy != null)
            enemy.TakeDamage(arma.dmg);
    }
}
