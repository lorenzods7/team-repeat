﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class CollectablePotion: MonoBehaviour
{

    public bool doesRotate = false;
    public int valor;
    public float velRotacion;

    void Update()
    {
        if (doesRotate)
        {
            gameObject.transform.Rotate(Vector3.up * Time.deltaTime * velRotacion);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            AudioSource source = GetComponent<AudioSource>();
            source.Play();
            //GameManager.instan.Collect(valor, gameObject);
        }
    }
}
